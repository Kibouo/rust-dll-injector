use std::ptr;

use winapi::um::{
    errhandlingapi::GetLastError,
    winbase::{FormatMessageW, FORMAT_MESSAGE_FROM_SYSTEM},
};

#[derive(Debug)]
pub(crate) struct WinError {
    _code: u32,
    _description: String, // info from Windows
}

impl WinError {
    pub fn new() -> Self {
        let error_code = unsafe { GetLastError() };

        let buff_size = 256;
        let mut buff: Vec<u16> = vec![0; buff_size];

        let chars_copied = unsafe {
            FormatMessageW(
                FORMAT_MESSAGE_FROM_SYSTEM,
                ptr::null(),
                error_code,
                0,
                buff.as_mut_ptr(),
                (buff_size + 1) as u32,
                ptr::null_mut(),
            )
        };

        let error_message = if chars_copied == 0 {
            // Not all errors have a string equivalent.
            // For an overview of mnemonics see: https://learn.microsoft.com/en-us/windows/win32/debug/system-error-codes--0-499-
            String::from("<No system error string found>")
        } else {
            String::from_utf16_lossy(&buff)
                .chars()
                .filter(|c| *c != '\r' && *c != '\n' && *c != '\0')
                .collect::<String>()
        };

        Self {
            _code: error_code,
            _description: error_message,
        }
    }
}
